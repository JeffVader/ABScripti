#!/bin/sh

# A simple wrapper script to convert ArchBang OB to emulate my ArchLabs i3 setup

# Initialise Pacman
sudo pacman-key --init
sudo pacman-key --populate
sudo pacman -Syyu archlinux-keyring

# Copy files into home directory, make keyhint.sh executable & move AL icomoon fonts to /usr/share/fonts/TTF
cp -a ~/ABScripti-main/Files/. ~
chmod +x ~/.config/i3/keyhint.sh
sudo mv ~/AL_icomoon.ttf /usr/share/fonts/TTF/

# Install official repository packages
sudo pacman -S --needed i3-wm i3status volumeicon xed noto-fonts-emoji arp-scan xfce4-terminal yad speedtest-cli ttf-font-awesome
read -p "Install extra official repository packages? (Y/n): " input
if [ "${input^^}" != "N" ]; then
  read -p "Edit list of extra official repository packages? (y/N): " input2
  [[ "${input2^^}" == "Y" ]] && sudo nano ~/repo_pkgs.txt
  sudo pacman -S --needed - < ~/repo_pkgs.txt
fi

# Uninstall unused AB packages
read -p "Uninstall unused AB packages? (Y/n): " input
[[ "${input^^}" != "N" ]] && sudo pacman -Rns gvim alacritty conky obconf openbox tint2

# Install & setup yay & install AUR packages
read -p "Install yay? (Y/n): " input
if [ "${input^^}" != "N" ]; then
  sudo pacman -S --needed base-devel git
  git clone https://aur.archlinux.org/yay.git
  cd yay && makepkg -si && cd ..
  yay --answerclean All --save
  yay --answerdiff None --save
  yay --sudoloop --save
  yay -S touchpad-indicator-git

  read -p "Install extra AUR packages? (Y/n): " input2
  if [ "${input2^^}" != "N" ]; then
    read -p "Edit list of extra AUR packages? (y/N): " input3
    [[  "${input3^^}" == "Y" ]] && sudo nano ~/aur_pkgs.txt
  
    yay -S --needed - < ~/aur_pkgs.txt
  fi
fi

# AL icons
read -p "Use ArchLabs icons? (Y/n): " input
[[ "${input^^}" != "N" ]] && curl -LJO https://gitlab.com/JeffVader/al/-/raw/main/archlabs-icons-1.4-2-x86_64.pkg.tar.zst &&
sudo pacman -U archlabs-icons-1.4-2-x86_64.pkg.tar.zst &&
sed -i '3s/.*/gtk-icon-theme-name=ArchLabs-Dark/g;4s/.*/gtk-font-name=DejaVu Sans 11/g' ~/.config/gtk-3.0/settings.ini

# Swap left and right mouse buttons & cursors
read -p "Use left-handed buttons & (mirrored) mouse icon? (Y/n): " input
[[ "${input^^}" != "N" ]] && sed -i 's/#exec --no-startup-id xmodmap -e/exec --no-startup-id xmodmap -e/g' ~/.config/i3/config &&
sudo mv /usr/share/icons/Adwaita/cursors/left_ptr /usr/share/icons/Adwaita/cursors/temp_ptr &&
sudo mv /usr/share/icons/Adwaita/cursors/right_ptr /usr/share/icons/Adwaita/cursors/left_ptr &&
sudo mv /usr/share/icons/Adwaita/cursors/temp_ptr /usr/share/icons/Adwaita/cursors/right_ptr

# Enable Bluetooth
read -p "Enable Bluetooth? (Y/n): " input
[[ "${input^^}" != "N" ]] && sudo pacman -S bluez bluez-utils blueman && 
sudo systemctl enable --now bluetooth

# Set CPU governor
read -p "Set CPU governor? (Y/n): " input
if [ "${input^^}" != "N" ]; then
  echo "pacman -S cpupower"
  echo "Select CPU governor:"
  select governor in ondemand performance powersave conservative userspace
  do
    [[ $REPLY -ge 1 && $REPLY -le 5 ]] && echo "sed -i '3s/.*/governor='$governor'/g' /etc/default/cpupower" && break
    echo "Invalid entry, please select CPU governor:"
  done
  echo "systemctl enable cpupower.service"
fi

# Autologin
read -p "Autologin? (Y/n): " input
[[ "${input^^}" != "N" ]] && sed -i "s/<USER>/$USER/g" ~/autologin.conf &&
sudo mkdir /etc/systemd/system/getty@tty1.service.d &&
sudo cp -a ~/autologin.conf /etc/systemd/system/getty@tty1.service.d/

# Make Exit Menu option selection case insensitive
sed -i '7s/rofi -dmenu -p/rofi -dmenu -p -i/g' ~/AB_Scripts/exitmenu

# Reboot
read -p "Reboot? (Y/n): " input
[[ "${input^^}" != "N" ]] && sudo reboot

exit
