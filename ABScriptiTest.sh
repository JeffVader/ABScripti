#!/bin/sh

# A simple wrapper script to convert ArchBang OB to emulate my ArchLabs i3 setup
# NB: To test script functionality - only echos commands

# Initialise Pacman
echo "pacman-key --init"
echo "pacman-key --populate"
echo "pacman -Syyu archlinux-keyring"

# Copy files into home directory & make keyhint.sh executable
echo "cp -a ~/ABScripti-main/Files/. ~"
echo "chmod +x ~/.config/i3/keyhint.sh"

# Install official repository packages
echo "pacman -S volumeicon xed noto-fonts-emoji arp-scan xfce4-terminal yad speedtest-cli ttf-font-awesome"
read -p "Install extra official repository packages? (Y/n): " input
if [ "${input^^}" != "N" ]; then
  read -p "Edit list of extra official repository packages? (Y/n): " input2
  [[ "${input2^^}" != "N" ]] && echo "nano ~/repo_pkgs.txt"
  echo "pacman -S --needed - < ~/repo_pkgs.txt"
fi

# Uninstall unused AB packages
read -p "Uninstall unused AB packages? (Y/n): " input
[[ "${input^^}" != "N" ]] && echo "pacman -Rns gvim alacritty conky obconf openbox tint2"

# Install & setup yay & install AUR packages
read -p "Install yay? (Y/n): " input
if [ "${input^^}" != "N" ]; then
  echo "pacman -S --needed base-devel git"
  echo "git clone https://aur.archlinux.org/yay.git"
  echo "cd yay && makepkg -si && cd .."
  echo "yay --answerclean All --save"
  echo "yay --answerdiff None --save"
  echo "yay -S touchpad-indicator-git"

  read -p "Install extra AUR packages? (Y/n): " input2
  if [ "${input2^^}" != "N" ]; then
    read -p "Edit list of extra AUR packages? (Y/n): " input3
    [[  "${input3^^}" != "N" ]] && echo "nano ~/aur_pkgs.txt"
  
    echo "yay -S --needed - < ~/aur_pkgs.txt"
  fi
fi

# AL icons
read -p "Use ArchLabs icons? (Y/n): " input
[[ "${input^^}" != "N" ]] && echo "curl -LJO https://github.com/ArchLabs/archlabs/raw/master/x86_64/archlabs-icons-1.4-2-x86_64.pkg.tar.zst" &&
echo "pacman -U archlabs-icons-1.4-2-x86_64.pkg.tar.zst" &&
echo "sed -i '3s/.*/gtk-icon-theme-name=ArchLabs-Dark/g;4s/.*/gtk-font-name=DejaVu Sans 11/g' ~/.config/gtk-3.0/settings.ini"

# Swap left and right mouse buttons & cursors
read -p "Use left-handed buttons & (mirrored) mouse icon? (Y/n): " input
[[ "${input^^}" != "N" ]] && echo "sed -i 's/#exec --no-startup-id xmodmap -e/exec --no-startup-id xmodmap -e/g' ~/.config/i3/config" &&
echo "mv /usr/share/icons/Adwaita/cursors/left_ptr /usr/share/icons/Adwaita/cursors/temp_ptr" &&
echo "mv /usr/share/icons/Adwaita/cursors/right_ptr /usr/share/icons/Adwaita/cursors/left_ptr" &&
echo "mv /usr/share/icons/Adwaita/cursors/temp_ptr /usr/share/icons/Adwaita/cursors/right_ptr"

# Enable Bluetooth
read -p "Enable Bluetooth? (Y/n): " input
[[ "${input^^}" != "N" ]] && echo "pacman -S bluez bluez-utils blueman" && 
echo "systemctl enable --now bluetooth"

# Set CPU governor
read -p "Set CPU governor? (Y/n): " input
if [ "${input^^}" != "N" ]; then
  echo "pacman -S cpupower"
  echo "Select CPU governor:"
  select governor in ondemand performance powersave conservative userspace
  do
    [[ $REPLY -ge 1 && $REPLY -le 5 ]] && echo "sed -i '3s/.*/governor='$governor'/g' /etc/default/cpupower" && break
    echo "Invalid entry, please select CPU governor:"
  done
  echo "systemctl enable cpupower.service"
fi

# Autologin
read -p "Autologin? (Y/n): " input
[[ "${input^^}" != "N" ]] && echo "sed -i s/<USER>/$USER/g ~/autologin.conf" &&
echo "mkdir /etc/systemd/system/getty@tty1.service.d" &&
echo "cp -a ~/autologin.conf /etc/systemd/system/getty@tty1.service.d/"

# Reboot
read -p "Reboot? (Y/n): " input
[[ "${input^^}" != "N" ]] && echo "reboot"

exit
