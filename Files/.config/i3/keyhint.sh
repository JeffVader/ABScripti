#!/bin/bash

yad --title="Keyhints" --no-buttons --geometry=270x860-10+30 --list --column="Key" --column="Binding" --no-headers \
"[S]+R    "       "Launcher" \
"[S]+F1    "       "Keyhints" \
" " " " \
"[S]+E"           "Editor" \
"[S]+F"           "File Manager" \
"[S]+T"           "Terminal" \
"[S]+W"           "Web Browser" \
" " " " \
"[S]+#"           "Focus WS #" \
"[S]+Shift+#"     " WS #" \
"[S]+"          "Change focus" \
"[S]+Shift+"    "Move Window" \
"[S]+Shift+H"     "Horiz. Split" \
"[S]+Shift+V"     "Vert. Split" \
"[S]+Q"           "Kill Window" \
" " " " \
"Ctrl+R"          "Resize" \
"         "      "Shrink Width" \
"         "      "Grow Width" \
"         "      "Shrink Height" \
"         "      "Grow Height" \
" " " " \
"[S]+Space"       "Floating" \
"      Ctrl+"    "Move Floated" \
" " " " \
"[S]+P"           "Hide Bars" \
"F11"             "Fullscreen" \
"[S]+Shift+B"     "Border " \
"[PrtScn]"        "Screenshot" \
"[S]+Shift+D"     " Scratchpad" \
"[S]+D"           "Scratchpad" \
" " " " \
"[S]+Shift+R"     "Restart i3" \
"[S]+X"           "System Exit" \

