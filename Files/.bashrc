# Add nano as default editor
export EDITOR=xed
export TERMINAL=xfce4-terminal
export BROWSER=firefox

# Add scripts path
export PATH=$PATH:~/AB_Scripts

alias ls='ls --color=auto'

alias la='ls -Ah'
alias ll='ls -lAh'
alias grep='grep --color=auto'
alias grub-update='sudo grub-mkconfig -o /boot/grub/grub.cfg'
alias mirror-update='sudo reflector --verbose --score 100 -l 50 -f 10 --sort rate --save /etc/pacman.d/mirrorlist'

### MY ALIASES ###

#ALIASES
alias aliases="nano ~/.bashrc"
alias alias-update=". ~/.bashrc"

#MISC
alias mirrors="sudo reflector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist"
alias uninstall="yay -Rns "
alias orphans='[[ -n $(pacman -Qdt) ]] && sudo pacman -Rs $(pacman -Qdtq) || echo "no orphans to remove"'


#NETWORK
alias netman="systemctl restart NetworkManager.service"
alias wifi="nmcli -f ssid,bssid,signal,bars,rate  dev wifi"
alias wifi-net="sudo arp-scan --interface=wlan0 --localnet --ignoredups"
alias lan-net="sudo arp-scan --interface=enp2s0f1 --localnet --ignoredups"

alias boot="xed ~/.xinitrc"

# Package sizes
alias pkg_size="expac -H M '%m\t%n' | sort -h"


