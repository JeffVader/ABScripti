# ABScripti

Simple Wrapper script to convert a CLEAN ArchBang installation to follow my old ArchLabs i3 setup (Super+F1 for keybinds, Esc to close it)

NB: This appears to be working for me but YMMV (I am a beginner learning)

To use: 
1. From a live ArchBang (https://archbang.org) USB stick, connect to internet and then install ArchBang, shutdown, remove USB stick & reboot into the CLEAN installed version. 
2. Connect to internet and execute the following in a bash terminal from Home directory (Downloads ABScripti.sh & files, unzips, makes script executable and runs it):
```
wget https://gitlab.com/JeffVader/ABScripti/-/archive/main/ABScripti-main.zip && unzip ABScripti-main.zip && chmod +x ~/ABScripti-main/ABScripti.sh && ~/ABScripti-main/ABScripti.sh
```
NB: Apart from the fact that the script uses Archbang as its base, 
lots of my stuff is borrowed &/or modified from the wonderful:

- ArchBang (https://archbang.org)
- ArchLabs (https://bitbucket.org/archlabslinux/archlabs/src/master/) - RIP
- EndeavourOS (https://endeavouros.com/)

### Actions & Prompts(?)
- Initialise Pacman
- Insert my Dot Files, Background, i3 & rofi Configs & Keybind Pop-Up
- Install required official repository packages (volumeicon xed noto-fonts-emoji arp-scan xfce4-terminal yad speedtest-cli)
- Install extra official repository packages? 
	- Edit list of extra official repository packages?
- Uninstall unused AB packages? (gvim alacritty conky obconf openbox tint2)
- Install yay? 
	- Install preferred AUR packages (touchpad-indicator-git joplin-appimage enpass-bin)
	- Edit list of extra AUR packages?
- Use ArchLabs icons?
- Use left-handed buttons & (mirrored) mouse icon? 
- Enable Bluetooth? 
- Set CPU governor? 
- Autologin?
- Reboot? 
